/*
    General view for general jobs
*/
folder('Pipelines') {
    description('Ejemplos de Pipelines')
}


def jobname = 'Pipelines/docker-build-pipeline'
pipelineJob(jobname) {

  logRotator {
      numToKeep(5)
  }
  definition {
    cpsScm {
      scm {
        git {
          remote {
                url("${CONF_REPO}")
            }
            branches("${RAMA_BASE}")
        }
      }
      scriptPath('pipelines/docker.Jenkinsfile')
      lightweight(false)
    }
  }
}