def jobname = 'ejemplo-3-github'
pipelineJob(jobname) {

  logRotator {
      numToKeep(3)
  }
  parameters {
    gitParam('GIT_BRANCH_NAME') {
        description 'The Git branch to checkout'
        type 'BRANCH'
        defaultValue 'origin/master'
        sortMode 'ASCENDING_SMART'
    }
    stringParam('REPO', 'https://github.com/ricardocutzhernandez/cicd_github_actions.git', 'repositorio a construir')
  }
  definition {
    cpsScm {
      scm {
        git {
          remote {
                url('${REPO}')
            }
            branches('${GIT_BRANCH_NAME}')
        }
      }
      scriptPath('Jenkinsfile')
      lightweight(false)
    }
  }
}