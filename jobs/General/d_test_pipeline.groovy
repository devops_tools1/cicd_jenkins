def folder_name = "General"

pipelineJob("schedule-scaling") {
    description("Jenkins pipeline for schedule scaling actions to services in the API")
    //concurrentBuild(allowConcurrentBuild = false)
    logRotator {
        numToKeep(5)
    }
    parameters {
        gitParam('GIT_BRANCH_NAME') {
            description 'The Git branch to checkout'
            type 'BRANCH'
            defaultValue 'origin/master'
            sortMode 'ASCENDING_SMART'
        }
        stringParam('REPO', 'https://gitlab.com/devops_tools1/cicd_jenkins', 'repositorio a construir')
        stringParam('IDENTIFIER', 'ottera', 'identifier used in the infrastructure')
        stringParam('ENVIRONMENT', "stage", 'environment used in the infrastructure')
        stringParam('ACTION', "CREATE", 'defines what action will be performed')
        stringParam('REGION', "us-west-2", 'region where the api is deployed')
        stringParam('GROUP', "group0", 'name of the api group')
        stringParam('SCALE_IN_MIN', '1', "minimum tasks to have when scaling in")
        stringParam('SCALE_IN_MAX', '3', "max tasks to have when scaling in")
        stringParam('SCALE_OUT_MIN', '3', "minimum tasks to have when scaling out")
        stringParam('SCALE_OUT_MAX', '6', "max tasks to have when scaling out")
        stringParam('CRON_EXPRESSION_SCALE_OUT', "0 16 * * ? *", "cron expression to schedule: check aws documentation for cron https://docs.aws.amazon.com/autoscaling/application/userguide/examples-scheduled-actions.html")
        stringParam('CRON_EXPRESSION_SCALE_IN', "20 16 * * ? *", "cron expression to schedule: check aws documentation for cron https://docs.aws.amazon.com/autoscaling/application/userguide/examples-scheduled-actions.html")
    }
    definition {
        cpsScm {
            scm {
                git {
                remote {
                        url('${REPO}')
                    }
                    branches('${GIT_BRANCH_NAME}')
                }
            }
            scriptPath('Jenkinsfile')
            lightweight(false)
        }
    }
}