/*
    General view for general jobs
*/

def jobname = 'Pipelines/node-app-pipeline'
pipelineJob(jobname) {

  logRotator {
      numToKeep(5)
  }
  parameters {
    gitParam('GIT_BRANCH_NAME') {
        description 'The Git branch to checkout'
        type 'BRANCH'
        defaultValue 'origin/main'
        sortMode 'ASCENDING_SMART'
    }
    stringParam('REPO', 'https://gitlab.com/devops_tools1/cicd_gitlab.git', 'repo publico a construir')
  }
  definition {
    cpsScm {
      scm {
        git {
          remote {
                url('${REPO}')
            }
            branches('${GIT_BRANCH_NAME}')
        }
      }
      scriptPath('Jenkinsfile')
      lightweight(false)
    }
  }
}